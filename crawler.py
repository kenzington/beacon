import re
import os
import sys
import time
import sqlite3
import pathlib
import logging
import argparse
import io

import streetview


from secrets import API_KEY

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("PIL.Image").setLevel(logging.WARNING)


def list_projects(cur: sqlite3.Cursor):
    for pid, project, desc in cur.execute("SELECT ID, NAME, DESCRIPTION FROM PROJECT"):
        len_remaining = 80 - len(str(pid)) - len(project) - 4 - 2
        print(f"{project}({pid}): "
                f"{(desc[:len_remaining] + '..') if desc is not None and len(desc) > len_remaining+2 else desc or ''}")


def find_project_id(cur: sqlite3.Cursor, id_or_name):
    if id_or_name is None:
        c = cur.execute("SELECT COUNT(*) FROM PROJECT")
        cnt = c.fetchone()[0]
        if cnt == 0:
            logging.error("No projects exist. Please insert a project into the database manually before continuing.")
            return
            #raise Exception("NO_PROJECT_EXISTS")
        if cnt > 1:
            logging.error("Multiple projects exist. Please specifiy a project name or id with --project. Use --list to list all projects.")
            return
            #raise Exception("AMBIGUOUS_PROJECT_SEARCH")
        c = cur.execute("SELECT ID FROM PROJECT LIMIT 1")
        return c.fetchone()[0]
    try:
        pid = int(id_or_name)
        c = cur.execute("SELECT ID FROM PROJECT WHERE ID = ?", (pid,))
        return c.fetchone()[0]
    except ValueError:
        c = cur.execute("SELECT ID FROM PROJECT WHERE NAME = ?", (id_or_name,))
        return c.fetchone()[0]


def read_input(reader: io.TextIOWrapper):
    for line in reader:
        line = line.strip()
        if not line:
            continue
        match = re.match(r'(?P<lat>-?\d+[.]\d+)[, ]+(?P<lon>-?\d+[.]\d+)([, ]+(?P<source>.+))?', line)
        if not match:
            print(f"Unable to parse line: {line}")
            continue
            
        yield (match.group('lat'), match.group('lon'), match.group('source'))


def crawl_locations(conn: sqlite3.Connection, cur: sqlite3.Cursor,
                    project_id: int, delay: float,
                    input_file: pathlib.Path, image_dir: pathlib.Path):
    with input_file.open(mode='r') as reader:
        for lat, lon, source in read_input(reader):
            # TODO add geospatial distance check for close but not identical locations
            exists = cur.execute("SELECT EXISTS(SELECT 1 FROM LOCATION WHERE PROJECT_ID=? AND LAT=? AND LON=?)", (project_id, lat, lon)).fetchone()
            exists_fail = cur.execute("SELECT EXISTS(SELECT 1 FROM FAILED_LOCATION WHERE PROJECT_ID=? AND LAT=? AND LON=?)", (project_id, lat, lon)).fetchone()

            if exists == (1,) or exists_fail == (1,):
                logging.debug(f"Location {lat}, {lon} already exists in project")
                continue
            
            panoids = streetview.panoids(lat=lat, lon=lon)[:1]
            if not panoids:
                print(f"No data for location {lat}, {lon}")
                cur.execute("INSERT INTO FAILED_LOCATION "
                            "(PROJECT_ID, LAT, LON, SOURCE) "
                            "VALUES (?, ?, ?, ?)",
                            (project_id, lat, lon, source))
                conn.commit()
                continue

            logging.debug(f"Looking up {lat}, {lon}")
            for panoid in panoids:
                cur.execute("INSERT INTO LOCATION "
                            "(PROJECT_ID, LAT, LON, SOURCE) "
                            "VALUES (?, ?, ?, ?)", (project_id, lat, lon, source))
                rid = cur.lastrowid
                
                for heading in (0, 120, 240):
                    fname = streetview.api_download(panoid['panoid'], heading, str(image_dir), API_KEY)
                    if fname == None:
                        logging.warning(f"Unable to find image at {lat} {lon}, heading {heading}.")
                        continue
                    cur.execute("INSERT INTO STREET_IMAGE "
                                "(LOCATION_ID, IMAGE_PATH, HEADING) "
                                "VALUES (?, ?, ?)", (rid, fname, heading))
                    time.sleep(delay)
            conn.commit()
            time.sleep(delay)


def main(args):
    with sqlite3.connect(str(args.database)) as conn:
        cur = conn.cursor()
        if args.list:
            list_projects(cur)
            return 1
        project_id = find_project_id(cur, args.project)
        if project_id is None:
            print(f"Could not find project '{args.project}'")
            return 1
        if not args.input.is_file():
            logging.error(f"Input file {args.input} does not exist.")
        if not args.image_dir.is_dir():
            args.image_dir.mkdir(parents=True, exist_ok=True)
        try:
            crawl_locations(conn, cur, project_id, args.delay, args.input, args.image_dir)
        except KeyboardInterrupt:
            conn.commit()
        finally:
            cur.close()
    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--database', type=pathlib.Path, default=pathlib.Path('data.sqlite'),
                        help="SQLite database file")
    parser.add_argument('--image-dir', type=pathlib.Path, default=pathlib.Path('images'),
                        help="Directory where newly loaded images will be stored. Relative to current dir.")
    parser.add_argument('-i', '--input', type=pathlib.Path, default=pathlib.Path('data.txt'),
                        help="Input file, requires comma-separated latitude "
                             "and longitude values. One per line.")
    parser.add_argument('-p', '--project',
                        help="Project namd or ID. If only one project exists, it will be used automatically.")
    parser.add_argument('-l', '--list', action='store_true',
                        help="List all projects and their IDs")
    parser.add_argument('--delay', type=float, default=0.5,
                        help="Amount of time to delay between requests. Prevents Google rate limit.")
    args = parser.parse_args()

    if not args.input.is_file():
        logging.error(f"--input file '{args.input}' cannot be found")
        
    if not args.database.is_file():
        logging.error(f"--database file '{args.database}' cannot be found")

    try:
        result = main(args)
    except Exception as e:
        logging.error('', exc_info=e)
    try:
        sys.exit(int(result))
    except:
        pass
