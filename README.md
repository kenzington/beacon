# Beacon

An application for vetting locations for relevance

## Installation

Create and a virtual environment:

```bash
python3 -m virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

Prep directories and move default files:

```bash
mkdir images
mkdir reference
cp data.sqlite.backup data.sqlite
cp secrets.default.py secrets.py
```

Add your Google Maps API key to `secrets.py`. You can get credentials by signing
up [here](https://console.cloud.google.com/apis/credentials).

Create a Project in the database for the work you're doing. Projects allow you
to separate groups of images/locations and track them individually.

```bash
sqlite3 data.sqlite "INSERT INTO PROJECT (NAME, DESCRIPTION) VALUES ('My First Project', 'Some Description')"
```

Capture the ID of your project and note it for later:

```bash
sqlite3 data.sqlite "SELECT ID, NAME FROM PROJECT"
# 1|My First Project
```

Copy a couple of reference images into the reference directory. These images
are what you are _looking_ for at each location.

Add those images to the database. Paths should be _relative_ to the root "beacon" folder:

```bash
sqlite3 data.sqlite "INSERT INTO REFERENCE_IMAGE (PROJECT_ID,  IMAGE_PATH) VALUES (1, 'reference/D_f2pzsWkAA6Ps3.jpeg')"
```

## Capture Locations

With folders and the database prepped, you can begin capturing images for your
locations. The crawler operates on an input file of comma-separated latitudes
and longitudes. There is also an optional third field on each line called
the "source". This can be used to document cross-reference information in
case you need to mark "completed" locations off of an external list. Example
input file, with sources:

```
59.90165985410574, 30.317232774805124, fid:1542
59.87228114137419, 30.316713789972663, fid:1543
59.872225073235995, 30.314627097100928, fid:1544
```

With a data file, begin collecting data.

```bash
python crawler.py --project 1 -i data.txt -d data.sqlite --image-dir images
```

![crawler example](screenshots/crawler-example.png)

## Start the Webapp

With some records in the database, it's time to start the webapp. Currently,
it's very bare-bones with little configuration. If you've changed the 
`reference` or `images` folder names or the database filename, you'll have
to patch the web app code to use the new locations. Additionally, we use
Flask's built-in webserver for code and static file hosting, which is not
recommended for production use but will suffice for our personal use.

Set the proper environment variables:

```bash
cd beaconapp/
export FLASK_APP=server  # this is the python filename of the server
#export FLASK_ENV=development  # optional - for debugging
```

Finally, start the server. With Flask installed in the virtualenv, it offers
a script to start the web app.

```bash
flask run
```

With the app running, open the web page in a browser (defaults to
http://127.0.0.1:5000/). You will see a list of your projects. Click on
the project you want to browse.

The project page will pick a location that has not been verified and display
the streetview images to you alongside the reference image(s) you added
to the project. If you need to see more detail, click on one of the
streetview images and it will open an InstantStreetview tab facing
the same direction where you can pan, zoom, and travel around. After you've
made a decision whether the location could possibly be a match for your reference
or not, click one of the form buttons at the bottom of the page. You
will automatically be shown a new location to verify. If you run out of
locations to verify, you will automatically be sent back to the project listing
page.

![webapp example](screenshots/webapp-example.png)

Verification details can be found in the `VERIFICATION` table:

```bash
# Run from the beaconapp directory, so database is one level up.
sqlite3 ../data.sqlite "SELECT LAT, LON, OUTCOME FROM VERIFICATION v JOIN LOCATION l ON v.LOCATION_ID = l.ID"
# 59.90165985410574|30.317232774805124|FAIL
# 59.87228114137419|30.316713789972663|FAIL
```
