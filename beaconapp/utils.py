import functools

from flask import request, g

def capture_ip(f):
    @functools.wraps(f)
    def wrap(*args, **kwds):
        if 'X-Forwarded-For' in request.headers:
            remote_addr = request.headers.getlist("X-Forwarded-For")[0].rpartition(' ')[-1]
        else:
            remote_addr = request.remote_addr or 'untrackable'
        g.remote_addr = remote_addr
        return f(*args, **kwds)
    return wrap