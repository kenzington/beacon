import os

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from ..utils import capture_ip

from .. import repository

bp = Blueprint('project', __name__)

@bp.route('/')
@capture_ip
def index():
    projects = repository.get_projects()
    return render_template('project/index.html.j2',
        projects=projects)


def get_project_or_fail(project_id):
    project = repository.get_project(project_id)
    if project is None:
        abort(404, "Project ID {0} doesn't exist.".format(project_id))
    return project


@bp.route('/<int:project_id>', methods=('GET',))
@capture_ip
def browse_project(project_id):
    loc = repository.get_random_unverified_location(project_id)
    if loc is not None:
        return redirect(url_for('project.browse_location', project_id=project_id, location_id=loc[0]), code=303)
    return redirect(url_for('project.index'), code=303)
    #return render_template('project/item.html.j2', project=project)


@bp.route('/<int:project_id>/location/<int:location_id>', methods=('GET', 'POST'))
@capture_ip
def browse_location(project_id, location_id):
    if request.method == 'POST':
        try:    
            outcome = "FAIL"
            maybe = request.form.get('possible_location')
            if maybe is not None:
                outcome = "POSSIBLE"
            fail = request.form.get('not_location')
            if fail is not None:
                outcome = "FAIL"
            need_more = request.form['need_more_info']

            repository.insert_verification(location_id, g.remote_addr, outcome, need_more)
        except Exception as e:
            abort(400, str(e))

        loc = repository.get_random_unverified_location(project_id)
        if loc is not None:
            return redirect(url_for('project.browse_location', project_id=project_id, location_id=loc[0]), code=303)
        return redirect(url_for('project.index'), code=303)

    loc = repository.get_location(location_id)
    refs = repository.get_reference_images(project_id)
    photos = repository.get_street_images(location_id)
    return render_template('project/location.html.j2', reference_images=refs, street_images=photos, location=loc)