from .db import get_db

def get_projects():
    return get_db().execute(
        "SELECT ID, NAME, DESCRIPTION FROM PROJECT ORDER BY NAME ASC"
    ).fetchall()


def get_project(project_id):
    return get_db().execute(
        "SELECT ID, NAME, DESCRIPTION FROM PROJECT WHERE ID = ?", (project_id,)
    ).fetchone()


def get_reference_images(project_id):
    return get_db().execute(
        "SELECT ID, IMAGE_PATH FROM REFERENCE_IMAGE WHERE PROJECT_ID = ?", (project_id,)
    ).fetchall()


def get_locations_for_project(project_id):
    return get_db().execute(
        "SELECT ID, LAT, LON, SOURCE FROM LOCATION WHERE PROJECT_ID = ?", (project_id,)
    ).fetchall()


def get_street_images(location_id):
    return get_db().execute(
        "SELECT ID, IMAGE_PATH, HEADING FROM STREET_IMAGE WHERE LOCATION_ID = ?", (location_id,)
    ).fetchall()


def get_location(location_id):
    return get_db().execute(
        "SELECT ID, LAT, LON, SOURCE FROM LOCATION WHERE ID = ?", (location_id,)
    ).fetchone()


def insert_verification(location_id, username, outcome, need_more_info):
    db = get_db()
    db.execute(
        "INSERT OR IGNORE INTO VERIFICATION "
        "(LOCATION_ID, USERNAME, OUTCOME, NEED_MORE_INFO)"
        "VALUES (?, ?, ?, ?)", (location_id, username, outcome, need_more_info)
    )
    db.commit()


def get_random_unverified_location(project_id):
    return get_db().execute(
        "SELECT l.ID, l.LAT, l.LON, l.SOURCE FROM LOCATION l "
        "WHERE NOT EXISTS(SELECT 1 FROM VERIFICATION v WHERE l.ID = v.LOCATION_ID) "
        "LIMIT 1"
    ).fetchone()