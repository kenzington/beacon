import os

from flask import Flask, send_from_directory


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=False)

    app.config.from_mapping(
        SECRET_KEY='wbshA9APEdPUGlZF',
        DATABASE=os.path.join(app.root_path, '..', 'data.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.init_app(app)

    from .views import project
    app.register_blueprint(project.bp)
    app.add_url_rule('/', endpoint='index')

    @app.route('/reference/<path:path>')
    def reference_images(path):
        return send_from_directory('../reference', path)

    @app.route('/images/<path:path>')
    def location_images(path):
        return send_from_directory('../images', path)

    return app

if __name__ == '__main__':
    create_app(None)